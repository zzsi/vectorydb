from fastapi.testclient import TestClient
from vectory_db.app import app
from vectory_db.data_models import IndexResponse


client = TestClient(app)


def test_index():
    # r = IndexResponse(_id="dummy_id", _index="default", extra="a")
    # print(r.dict())
    res = client.post("/index", json={"_id": "123", "_index": "default"})
    assert res.status_code == 500
    error_msg = res.content.decode("utf-8")
    assert error_msg.startswith("Internal server error: A vector field is required.")

    response = client.post(
        "/index", json={"_id": "123", "_index": "default", "vector": [1, 2, 3]}
    )
    assert response.status_code == 200, f"{response.content.decode('utf-8')}"
    # res_dict = response.json()
    # assert res_dict["_id"] == "dummy_id"
    # assert res_dict["_index"] == "default"
