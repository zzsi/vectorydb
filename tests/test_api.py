from typing import List
import pytest
from vectory_db.api import API
from vectory_db.document_store.base import DocumentStore
from vectory_db.vector_store.base import VectorStore
from vectory_db.predictor.base import PredictionPipeline


@pytest.fixture(name="api_instances")
def get_api_with_different_configurations() -> API:
    api_with_base_stores = API(
        document_store=DocumentStore(),
        vector_store=VectorStore(),
        prediction_pipeline=PredictionPipeline(),
    )
    return [api_with_base_stores]


def test_collection_ops(api_instances: List[API]):
    for api in api_instances:
        api.create_collection(name="my_collection")
        assert api.collection_names == ["my_collection"]
        api.create_collection(name="my_collection2")
        assert api.collection_names == ["my_collection", "my_collection2"]
        api.drop_collection(name="my_collection")
        assert api.collection_names == ["my_collection2"]


def test_vector_ops_for_base_stores(api_instances: List[API]):
    for api in api_instances:
        api.insert_vector(collection="my_collection", vector=[1.2, 0, -2])
        assert api.count_vectors(collection="my_collection", field=None) == 1
        with pytest.raises(IndexError):
            api.count_vectors(collection="my_collection2", field=None)
        api.insert_vector(collection="my_collection", vector=[2, 1, -2])
        assert api.count_vectors(collection="my_collection", field=None) == 2
        assert api.count_vectors(collection="my_collection", field="") == 2
        assert (
            api.count_vectors(collection="my_collection", field="a_random_field") == 0
        )
        api.insert_vector(collection="my_collection2", vector=[3, 0, -2])
        api.insert_vector(collection="my_collection2", vector=[3, 0, -2])
        api.insert_vector(
            collection="my_collection2", vector=[3, 0, -2], field="column1"
        )
        assert api.count_vectors(collection="my_collection2", field="column1") == 1
        api.drop_collection("my_collection")
        with pytest.raises(IndexError):
            api.count_vectors("my_collection")


def test_vector_similarity_for_base_stores(api_instances: List[API]):
    for api in api_instances:
        col = "my_collection"
        api.insert_vector(collection=col, vector=[1.2, 0, -2], doc_id="1")
        api.insert_vector(collection=col, vector=[8, 10, 3], doc_id=2)
        api.insert_vector(collection=col, vector=[0, 0, 0], doc_id=3)
        api.insert_vector(collection=col, vector=[3, -2, 1], doc_id="4")
        # TODO: duplicate doc is is ok for now.
        doc_ids_and_scores = api.find_nearest_neighbors(
            collection=col, vector=[3, -2, 1], limit=2
        )
        doc_id, score = doc_ids_and_scores[0]
        assert doc_id == "4"
        assert score >= 0.5, str(doc_ids_and_scores)

        doc_ids_and_scores = api.find_nearest_neighbors(
            collection=col, vector=[1, 1, 1], limit=2
        )
        doc_id, score = doc_ids_and_scores[0]
        assert doc_id == "2"
        assert score >= 0.5, str(doc_ids_and_scores)


def test_document_ops_for_base_stores(api_instances: List[API]):
    for api in api_instances:
        col = "collection1"
        api.create_collection(col)
        doc = {"a": 1}
        api.index(docs=[doc], collection=col)
