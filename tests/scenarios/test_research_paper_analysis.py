"""
For a folder of arxiv pdf files associated with the metadata (abstract etc.)
saved in corresponding txt, it would be great to ingest the data and show
trending topics, find similar papers given one that is being looked at, find
similar graphs/plots.
"""
