from typing import Optional
from fastapi import FastAPI, UploadFile, File

app = FastAPI()
# https://github.com/semi-technologies/t2v-transformers-models/blob/main/app.py


@app.post("/predict")
async def score_file(file: UploadFile = File(...)):
    return {"score": 0}


@app.post("/vectors/")
async def read_item(item: VectorInput, response: Response):
    try:
        vector = await vec.vectorize(item.text, item.config)
        return {"text": item.text, "vector": vector.tolist(), "dim": len(vector)}
    except Exception as e:
        response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        return {"error": str(e)}


@app.get("/health")
def route():
    return {"status": "ok"}
