"""RESTFUL API for vectory_db.
"""

import traceback
import uvicorn
from fastapi import FastAPI
from starlette.requests import Request
from starlette.responses import Response


from .data_models import IndexRequest, IndexResponse
from .milvus_client import MilvusClient

app = FastAPI()


async def catch_exceptions_middleware(request: Request, call_next):
    try:
        return await call_next(request)
    except Exception as e:  # pylint: disable=broad-except
        # you probably want some kind of logging here
        return Response(
            f"Internal server error: {e}. Stack trace: {traceback.format_exc()}",
            status_code=500,
        )


# https://stackoverflow.com/questions/61596911/catch-exception-globally-in-fastapi
app.middleware("http")(catch_exceptions_middleware)


@app.post("/index")
def index(req: IndexRequest) -> dict:
    client = MilvusClient()
    data = req.dict(by_alias=True)
    data.pop("_index", None)
    # TODO: the _id field is not created in the schema yet.
    client.insert(index=req.index, data=data)
    return IndexResponse(_id="dummy_id", _index="default").dict(by_alias=True)


if __name__ == "__main__":
    uvicorn.run(app, host="127.0.0.1", port=8000)
