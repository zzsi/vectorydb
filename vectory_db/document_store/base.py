from dataclasses import dataclass
from ..constants import doc_id_field


@dataclass
class CollectionCreationOptions:
    default_vector_field: str = None
    fields: dict = None
    vector_dimension: int = None


class DocumentStore:
    def __init__(self):
        self.collections = {}

    def create_collection(self, name: str, options: CollectionCreationOptions):
        self.collections[name] = {"options": options, "docs": []}

    def drop_collection(self, name: str):
        self.collections.pop(name, None)

    @property
    def collection_names(self):
        return list(self.collections.keys())

    def insert(self, collection: str, doc: dict):
        docs = self.collections.get(collection, {})
        doc_id = doc[doc_id_field()]
        docs[doc_id] = doc
        self.collections[collection] = docs
