from dataclasses import dataclass
from enum import Enum
from typing import Any


class Predicate(Enum):
    EQUAL = 1
    CONTAIN = 2
    NOT_EQUAL = 3
    GREATER_THAN = 4
    LESS_THAN = 5
    GREATER_THAN_OR_EQUAL = 6
    LESS_THAN_OR_EQUAL = 7
    VLIKE = 8


@dataclass
class Filter:
    column: str
    predicate: str
    prediction_target: str
    predictor: str


@dataclass
class VectorQuery:
    column: str
    query: Any
    type: str
    predictor: str
    min_similarity: float = 0.3
