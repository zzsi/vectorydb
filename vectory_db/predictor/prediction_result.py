from dataclasses import dataclass


@dataclass
class PredictionResult:
    """`output_from_predictors` holds individual prediction results from predictors.
    Example:
    {
        resnet18_final_fc: <vector>,
        resnet50_classification: <dict[class_name, score]>
    }

    `vector` is the canonical vector representation.
    """

    vector: list
    output_from_predictors: dict

    def enrich(self, doc):
        return {**doc, **self.output_from_predictors}
