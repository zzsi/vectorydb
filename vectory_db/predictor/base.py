from typing import List
from .prediction_result import PredictionResult


class PredictionStep:
    def run(self, docs: List[dict], result: List[PredictionResult]):
        pass


class PredictionPipeline:
    """A PredictionPipeline may contain sequential steps.
    The PredictionResult is passed around and gets updated by various
    steps.
    """

    steps: List[PredictionStep] = []

    def run(self, docs: List[dict]) -> List[PredictionResult]:
        results = [
            PredictionResult(vector=None, output_from_predictors={})
            for _ in range(len(docs))
        ]
        for prediction_step in self.steps:
            prediction_step.run(docs, results)
        return results
