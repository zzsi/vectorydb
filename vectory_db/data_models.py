from enum import Enum
from typing import List, Optional, Union
from pydantic import BaseModel, Field, Extra


class IndexRequest(BaseModel, extra=Extra.allow):
    """
    Request model for index endpoint.
    """

    id: Optional[Union[str, int]] = Field(alias="_id")
    index: Optional[str] = Field("default", alias="_index")
    vector: Optional[List[float]]


class IndexResponse(BaseModel, extra=Extra.allow):
    """
    Response model for index endpoint.
    """

    id: Optional[Union[str, int]] = Field(alias="_id")
    index: str = Field(alias="_index")


class FieldType(Enum):
    """
    Enum for data type.
    """

    INT64 = "INT64"
    FLOAT_VECTOR = "FLOAT_VECTOR"
    VARCHAR = "VARCHAR"
    DOUBLE = "DOUBLE"


class FieldSchema(BaseModel, extra=Extra.allow):
    name: str
    description: Optional[str] = ""
    type: FieldType
    is_primary: bool = False
    auto_id: bool = False
    max_length: int = 0
    dim: int = -1

    def to_milvus_field_schema(self):
        import pymilvus as mv

        mapping = self.get_dtype_mapping()
        if self.type == FieldType.FLOAT_VECTOR:
            return mv.FieldSchema(
                name=self.name,
                dtype=mapping[self.type],
                description=self.description,
                is_primary=self.is_primary,
                auto_id=self.auto_id,
                dim=self.dim,
            )
        elif self.type == FieldType.VARCHAR:
            return mv.FieldSchema(
                name=self.name,
                dtype=mapping[self.type],
                description=self.description,
                is_primary=self.is_primary,
                auto_id=self.auto_id,
                max_length=self.max_length,
            )
        else:
            return mv.FieldSchema(
                name=self.name,
                dtype=mapping[self.type],
                description=self.description,
                is_primary=self.is_primary,
                auto_id=self.auto_id,
            )

    def get_dtype_mapping(self) -> dict:
        import pymilvus as mv

        return {
            FieldType.INT64: mv.DataType.INT64,
            FieldType.FLOAT_VECTOR: mv.DataType.FLOAT_VECTOR,
            FieldType.VARCHAR: mv.DataType.VARCHAR,
            FieldType.DOUBLE: mv.DataType.DOUBLE,
        }


class Schema(BaseModel, extra=Extra.allow):
    fields: List[FieldSchema]
    description: Optional[str] = ""

    def to_milvus_collection_schema(self):
        import pymilvus as mv

        return mv.CollectionSchema(
            [f.to_milvus_field_schema() for f in self.fields],
            description=self.description,
        )
