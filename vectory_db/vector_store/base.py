from uuid import uuid4
import numpy as np
import heapq

from numpy.lib.arraysetops import isin


class VectorStore:
    def __init__(self):
        self.vectors = {self.default_collection: []}

    @property
    def default_collection(self) -> str:
        return "_default_collection_"

    @property
    def auto_doc_id(self) -> str:
        return str(uuid4())

    @property
    def default_field(self):
        return ""

    def _ensure_doc_id(self, doc_id) -> str:
        doc_id = doc_id or self.auto_doc_id
        if not isinstance(doc_id, str):
            doc_id = str(doc_id)
        return doc_id

    def _ensure_field(self, field) -> str:
        field = field or self.default_field
        if not isinstance(field, str):
            field = str(field)
        return field

    def insert(
        self,
        vector: list,
        collection: str = None,
        doc_id: str = None,
        field: str = None,
    ):
        collection = collection or self.default_collection
        self.vectors[collection] = self.vectors.get(collection, [])
        doc_id, field = self._ensure_doc_id(doc_id), self._ensure_field(field)
        self.vectors[collection].append([vector, doc_id, field])

    def count(self, collection: str, field: str = None) -> int:
        if collection not in self.vectors:
            raise IndexError(f"collection {collection} not found in the vector store.")
        vector_collection: list = self.vectors[collection]
        field = self._ensure_field(field)
        vectors_for_this_field = [v for v in vector_collection if v[2] == field]
        return len(vectors_for_this_field)

    def drop_collection(self, collection: str):
        self.vectors.pop(collection, None)

    def query(
        self,
        collection: str,
        vector: list,
        top_n: int = 5,
        field: str = None,
        doc_ids: list = None,
    ):
        """
        `doc_id_list` is used to filter the result.
        """
        vector_collection: list = self.vectors[collection]
        field = self._ensure_field(field)
        if doc_ids:
            doc_id_set = set(doc_ids)
            candidate_vectors = [
                vec
                for vec, doc_id, _field in vector_collection
                if doc_id in doc_id_set and field == _field
            ]
            candidate_doc_ids = [
                doc_id
                for _, doc_id, _field in vector_collection
                if doc_id in doc_id_set and field == _field
            ]
        else:
            candidate_vectors = candidate_vectors = [
                vec for vec, _, _field in vector_collection if field == _field
            ]
            candidate_doc_ids = [
                doc_id for _, doc_id, _field in vector_collection if field == _field
            ]
        sims = self._cosine_similarity(vector, candidate_vectors)
        sims_dict = {doc_id: sim for doc_id, sim in zip(candidate_doc_ids, sims)}
        keys_sorted = heapq.nlargest(top_n, sims_dict, key=sims_dict.get)
        return [(k, sims_dict[k]) for k in keys_sorted]

    def _cosine_similarity(self, vector, candidate_vectors) -> np.array:
        candidate_vectors = np.array(candidate_vectors)  # n x d
        vector = np.array(vector).reshape([-1, 1])  # d x 1
        norm = np.linalg.norm(candidate_vectors, axis=1)  # n x 1
        norm = np.maximum(norm, 1e-6)
        candidate_vectors /= np.tile(
            np.expand_dims(norm, -1), (1, candidate_vectors.shape[1])
        )
        vec_norm = np.linalg.norm(vector)
        vec_norm = np.maximum(vec_norm, 1e-6)
        vector = vector / float(vec_norm)
        sims = candidate_vectors.dot(vector)
        return sims
