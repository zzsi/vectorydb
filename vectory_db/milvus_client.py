import os
from typing import List, Optional

import numpy as np
import pymilvus as mv
from .data_models import Schema, FieldSchema, FieldType


class MilvusClient:
    """
    https://milvus.io/docs/product_faq.md#Can-vectors-with-duplicate-primary-keys-be-inserted-into-Milvus
    Milvus does not check for duplicate primary keys. So the client needs to ensure that the primary keys are unique.

    TODO: We may need to keep an elasticsearch index for non-vector fields. Documents are first inserted into Elasticsearch,
    and then inserted into Milvus.
    """

    def __init__(self, host=None, port=19530):
        if os.environ.get("MILVUS_HOST") is not None:
            host = os.environ.get("MILVUS_HOST")
        else:
            host = host or "localhost"
        mv.connections.connect("default", host=host, port=port)

    def insert(self, index: str, data: dict) -> List[int]:
        data = {k: v for k, v in data.items() if v is not None}
        assert (
            data.get("vector") is not None
        ), f"A vector field is required. Got keys: {data}"
        schema = self.get_schema(index)
        if schema is None:
            schema = self.infer_schema(data)
            schema = schema.to_milvus_collection_schema()
        # TODO: check schema types. The inferred schema should be a subset of the existing schema.
        col = self.get_collection(index)
        if col is None:
            col = mv.Collection(index, schema, consistency_level="Strong")
        # TODO: when should indexing happen? It may be expensive.
        print("******************************** schema")
        print(schema)
        # Milvus Collection supports inserting a list of columns. Each column has n_documents elements.
        columns = [[data[field.name]] for field in schema.fields]
        print("******************************** columns")
        print(columns)
        print("primary field in the collection:", col.primary_field)
        n_docs_before = col.num_entities
        res = col.insert(columns)
        n_docs_after = col.num_entities
        print(
            f"{res.insert_count} documents inserted. Primary keys: {res.primary_keys}"
        )
        print(f"collection now has {n_docs_after} documents")
        assert res.insert_count == 1
        assert n_docs_after == n_docs_before + res.insert_count
        return res

    def search(
        self,
        index: str,
        vectors: List[List[float]],
        expression: str,
        output_fields: List[str] = None,
        limit: int = 10,
    ):
        col = self.get_collection(index)
        if col is None:
            return []
        # TODO: consider storing col as a class member.
        col.load()
        search_param = {
            "data": vectors,
            "anns_field": "vector",
            "param": {"metric_type": "L2", "params": {"nprobe": 10}},
            "limit": limit,
            "expr": expression,
            "output_fields": output_fields,
        }
        res = col.search(**search_param)
        return res

    def get_schema(self, collection: str) -> Schema:
        return None

    def get_collection(self, index: str):
        if mv.has_collection(index):
            return mv.Collection(index)

    def infer_schema(self, data: dict) -> Schema:
        field_schemas = [
            self._infer_field_schema(k, v) for k, v in data.items() if v is not None
        ]
        return Schema(fields=field_schemas, description="")

    def _infer_field_schema(self, key: str, value) -> FieldSchema:
        if key == "_id":
            if isinstance(value, str):
                return FieldSchema(
                    name=key,
                    type=FieldType.VARCHAR,
                    is_primary=True,
                    auto_id=False,
                    max_length=100,
                )
            elif isinstance(value, int):
                return FieldSchema(
                    name=key,
                    type=FieldType.INT64,
                    is_primary=True,
                    auto_id=False,
                    max_length=100,
                )
            else:
                raise ValueError(f"_id must be int or str, got {type(value)}")

        if isinstance(value, float):
            return FieldSchema(name=key, type=FieldType.DOUBLE)

        if isinstance(value, str):
            return FieldSchema(name=key, type=FieldType.VARCHAR, max_length=200)

        if key == "vector":
            if isinstance(value, list):
                value = np.array(value, dtype=np.float32)
            if isinstance(value, np.ndarray):
                return FieldSchema(
                    name=key, type=FieldType.FLOAT_VECTOR, dim=value.size
                )

        raise ValueError(f"Unsupported type for field '{key}': {type(value)}")
