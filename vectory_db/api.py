from typing import Union, Any, List
from uuid import uuid4
import numpy as np
from .constants import doc_id_field
from .predictor.base import PredictionPipeline
from .vector_store.base import VectorStore
from .request_types import VectorQuery, Filter, Predicate
from .document_store.base import CollectionCreationOptions, DocumentStore

"""
Metadata about collections, and documents are saved to relational db.
"""


class API:
    def __init__(
        self,
        document_store: DocumentStore,
        vector_store: VectorStore,
        prediction_pipeline: PredictionPipeline,
    ):
        self.document_score = document_store
        self.vector_store = vector_store
        self.prediction_pipeline = prediction_pipeline

    # TODO: offload most methods to a Collection class.
    def create_collection(self, name: str, options: CollectionCreationOptions = None):
        self.document_score.create_collection(name, options)

    @property
    def collection_names(self):
        return self.document_score.collection_names

    def drop_collection(self, name: str):
        self.document_score.drop_collection(name)
        self.vector_store.drop_collection(name)

    def insert_vector(
        self, collection: str, vector: list, doc_id: str = None, field: str = None
    ):
        """
        It is possible to just insert a vector, without providing the doc.

        If doc_id is None, a uuid is created for this vector.
        """
        self.vector_store.insert(
            vector=vector, collection=collection, doc_id=doc_id, field=field
        )

    def delete(self, collection: str, doc_ids: List):
        """All vectors associated with the given doc_ids, for all fields,
        are deleted.
        """
        self.vector_store.delete(collection, doc_ids)
        self.document_score.delete(collection, doc_ids)

    def index(self, collection: str, docs: List[dict]):
        # Includes encoding.
        predictions = self.prediction_pipeline.run(docs)
        for doc, prediction in zip(docs, predictions):
            doc_id = self.get_or_create_doc_id(doc)
            doc[doc_id_field()] = doc_id
            enriched_doc = prediction.enrich(doc)
            self.document_score.insert(collection=collection, doc=enriched_doc)
            for field in doc.keys():
                if field == doc_id_field():
                    continue
                vector = prediction.vector
                self.vector_store.insert(
                    collection=collection, vector=vector, doc_id=doc_id, field=field
                )

    def get_or_create_doc_id(self, doc):
        doc_id = doc.get(doc_id_field())
        if doc_id is None:
            doc_id = str(uuid4())
            doc[doc_id_field()] = doc_id
        return doc_id

    def count_vectors(self, collection: str, field: str = None):
        return self.vector_store.count(collection, field)

    def find_nearest_neighbors(
        self, collection: str, vector: list, field: str = None, limit: int = 5
    ):
        return self.vector_store.query(
            collection=collection, vector=vector, top_n=limit
        )

    def search(
        self,
        collection: str,
        vector_queries: List[VectorQuery],
        filters: List[Filter],
        limit: int = 5,
        offset: int = 0,
    ):
        docs = self.document_score.filter(collection=collection, filters=filters)
        doc_ids = [doc[doc_id_field()] for doc in docs]
        vector_results_agg = {}
        for vectory_query in vector_queries:
            vector_results = self.vector_store.query(
                collection=collection, vectory=vectory_query.query, doc_ids=doc_ids
            )
            vector_results_agg = self._update_vector_results(
                vector_results_agg, vector_results
            )
        vector_results_agg["docs"] = [
            self.document_score.get_doc(doc_id=doc_id)
            for doc_id in vector_results_agg["doc_ids"]
        ]
        return vector_results_agg

    def _update_vector_results(self, vector_results_agg, vector_results):
        # TODO: a placeholder for now
        vector_results_agg = vector_results
        return vector_results_agg


"""
find(collection="t", filters={"image1": {"contains": "cat", "prediction_target": "tags", "predictor": "resnet18"}})
find(collection="t", vector_queries={"image1": {"predictor": "resnet18-last-fc", "query": pil_img, "min_similarity": 0.6}})
"""
