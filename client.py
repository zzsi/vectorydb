from dataclasses import dataclass
from typing import Union, Any, List
import numpy as np
from enum import Enum


@dataclass
class CollectionCreationOptions:
    vector_dimension: int = None


def create_collection(name: str, options: CollectionCreationOptions):
    """TODO: use grpc stub. Also allow using RESTful."""
    pass


def insert(collection: str, doc: dict, vector: Union[list, np.array]):
    pass


def index(collection: str, doc: dict):
    # Includes encoding.
    pass


class Predicate(Enum):
    EQUAL = 1
    CONTAIN = 2
    NOT_EQUAL = 3
    GREATER_THAN = 4
    LESS_THAN = 5
    GREATER_THAN_OR_EQUAL = 6
    LESS_THAN_OR_EQUAL = 7
    VLIKE = 8


@dataclass
class Filter:
    column: str
    predicate: str
    prediction_target: str
    predictor: str


@dataclass
class VectorQuery:
    column: str
    query: Any
    type: str
    predictor: str
    min_similarity: float = 0.3


def search(
    collection: str,
    vector_queries: List[VectorQuery],
    filters: List[Filter],
    limit: int = 5,
    offset: int = 0,
):
    pass


"""
find(collection="t", filters={"image1": {"contains": "cat", "prediction_target": "tags", "predictor": "resnet18"}})
find(collection="t", vector_queries={"image1": {"predictor": "resnet18-last-fc", "query": pil_img, "min_similarity": 0.6}})
"""
