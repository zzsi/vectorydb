```{shell}
poetry config virtualenvs.in-project true
```

The virtualenv will be created inside the project path and vscode will recognize.

If you already have created your project, you need to re-create the virtualenv to make it appear in the correct place:

```
poetry env list  # shows the name of the current environment
poetry env remove <current environment>
poetry install  # will create a new environment using your updated configuration
```


## Testing

### Run tests within `docker`

First start the required services:

```
docker-compose -f services/milvus/docker-compose.yml up -d minio etcd standalone
```

then run `bash bin/test.sh`, which does the following:

```{shell}
docker-compose -f services/milvus/docker-compose.yml up test
```

### Run tests outside of `docker`

Or, if you like to run tests outside of the docker container:

```{shell}
poetry run pytest tests [optional arguments]
```
